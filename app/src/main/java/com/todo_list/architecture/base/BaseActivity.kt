package com.todo_list.architecture.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.todo_list.R
import com.todo_list.architecture.events.EventObserver

abstract class BaseActivity<V : BaseViewModel<E>, E> : AppCompatActivity() {

    protected abstract val viewModel: V
    protected abstract val layout: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout)
        subscribeToEvents()
    }

    private fun subscribeToEvents() {
        viewModel.eventsLiveData.observe(this, EventObserver {
            it?.let { onEvent(it) }
        })
    }

    protected open fun onEvent(event: E) {}

    open fun showLoading(show: Boolean) {}
}