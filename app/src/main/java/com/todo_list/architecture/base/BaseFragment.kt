package com.todo_list.architecture.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.todo_list.architecture.events.EventObserver

abstract class BaseFragment<V : BaseViewModel<E>, E> : Fragment() {
    protected abstract val viewModel: V

    protected abstract val layout: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        subscribeToEvents()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(layout, container, false)
    }

    private fun subscribeToEvents() {
        viewModel.eventsLiveData.observe(this, EventObserver {
            it?.let { onEvent(it) }
        })

        viewModel.logout.observe(this, EventObserver {
            if (it) {
                logout()
            }
        })

        viewModel.progressVisibility.observe(this, EventObserver{
            showLoading(it)
        })
    }

    protected open fun logout() {}

    protected open fun onEvent(event: E) {}

    protected open fun showLoading(show: Boolean) {
        (activity as? BaseActivity<*, *>)?.showLoading(show)
    }

    override fun onDestroyView() {
        showLoading(false)
        super.onDestroyView()
    }
}