package com.todo_list.architecture.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.*
import com.todo_list.architecture.events.Event
import com.todo_list.repository.Repository

open class BaseViewModel<E>() : ViewModel() {

    protected val repository = Repository

    protected val _eventsLiveData: MutableLiveData<Event<E>> = MutableLiveData()
    val eventsLiveData: LiveData<Event<E>>
        get() = _eventsLiveData

    val logout = MutableLiveData<Event<Boolean>>()

    val progressVisibility = MutableLiveData<Event<Boolean>>()

    fun launchCoroutine(block: suspend () -> Unit, handler: (Throwable) -> Unit = {}): Job {
        val coroutineExceptionHandler = CoroutineExceptionHandler { _, throwable ->
            handler.invoke(throwable)
        }
        return viewModelScope.launch(coroutineExceptionHandler) {
            block.invoke()
        }
    }

    protected open fun logout() {
        repository.clear()
        logout.postValue(Event(true))
    }

    protected fun showLoading() {
        progressVisibility.postValue(Event(true))
    }

    protected fun hideLoading() {
        progressVisibility.postValue(Event(false))
    }
}