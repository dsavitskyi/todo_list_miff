package com.todo_list.architecture.extensions

import android.text.Editable
import android.view.View
import android.widget.EditText
import com.todo_list.utils.SimpleTextWatcher

private const val CLICK_DELAY_MILLIS = 500L

fun View.setThrottleOnClickListener(callback: (view: View) -> Unit) {
    var lastClickTime = 0L

    this.setOnClickListener {
        val currentTimeMillis = System.currentTimeMillis()

        if (currentTimeMillis - lastClickTime > CLICK_DELAY_MILLIS) {
            lastClickTime = currentTimeMillis
            callback.invoke(it)
        }
    }
}

fun EditText.afterTextChanged(callback: (String) -> Unit) =
    this.addTextChangedListener(object : SimpleTextWatcher() {
        override fun afterTextChanged(text: Editable) {
            super.afterTextChanged(text)
            callback(text.toString())
        }
    })

fun View.visible() {
    if (visibility != View.VISIBLE) {
        visibility = View.VISIBLE
    }
}

fun View.gone() {
    if (visibility != View.GONE) {
        visibility = View.GONE
    }
}

fun View.invisible() {
    if (visibility != View.INVISIBLE) {
        visibility = View.INVISIBLE
    }
}

