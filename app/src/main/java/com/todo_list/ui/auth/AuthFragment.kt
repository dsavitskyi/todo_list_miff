package com.todo_list.ui.auth

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.todo_list.R
import com.todo_list.app.Injector
import com.todo_list.architecture.base.BaseFragment
import com.todo_list.architecture.extensions.hasNetworkConnection
import com.todo_list.architecture.extensions.toast
import kotlinx.android.synthetic.main.register_fragment.*

class AuthFragment : BaseFragment<AuthViewModel, AuthFragment.Events>() {

    override val viewModel: AuthViewModel by viewModels {
        Injector.provideRegisterViewModelFactory()
    }

    override val layout by lazy { R.layout.register_fragment }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindUI()
    }

    override fun onEvent(event: Events) {
        super.onEvent(event)
        when(event) {
            Events.AUTH_ALL_FIELDS_ERROR -> toast(getString(R.string.txt_fill_all_the_fields))
            Events.AUTH_EMAIL_ERROR -> toast(getString(R.string.txt_valid_email))
            Events.AUTH_PASSWORD_ERROR -> toast(getString(R.string.txt_min_pass_length))
            Events.REGISTRATION_FAILED -> toast(getString(R.string.txt_registration_failed))
            Events.LOGIN_FAILED -> toast(getString(R.string.txt_login_faled))
            Events.REGISTRATION_SUCCESS -> {
                findNavController().navigate(AuthFragmentDirections.actionAuthFragmentToHomeFragment())
            }
        }
    }

    private fun bindUI() {
        signup_btn.setOnClickListener {
            userAuth(auth = {authEmail, authPassword ->  viewModel.signUp(authEmail, authPassword)})
        }

        login_btn.setOnClickListener {
            userAuth(auth = {authEmail, authPassword ->  viewModel.signIn(authEmail, authPassword)})
        }
    }

    private fun userAuth(auth: (email: String, password: String) -> Unit) {
        val email: String = email_edt_text.text.toString()
        val password: String = pass_edt_text.text.toString()

        if (requireContext().hasNetworkConnection()) {
            auth.invoke(email, password)
        } else {
            toast(getString(R.string.txt_no_network_connection))
        }
    }

    enum class Events {
        AUTH_ALL_FIELDS_ERROR,
        AUTH_EMAIL_ERROR,
        AUTH_PASSWORD_ERROR,
        REGISTRATION_FAILED,
        REGISTRATION_SUCCESS,
        LOGIN_FAILED
    }
}