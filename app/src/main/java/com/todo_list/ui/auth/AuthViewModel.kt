package com.todo_list.ui.auth

import android.text.TextUtils
import com.google.firebase.auth.FirebaseAuth
import com.todo_list.architecture.base.BaseViewModel
import com.todo_list.architecture.events.Event
import com.todo_list.utils.TextUtils.isPasswordValid
import com.todo_list.utils.TextUtils.isValidEmail

class AuthViewModel : BaseViewModel<AuthFragment.Events>() {

    private var auth: FirebaseAuth = FirebaseAuth.getInstance()

    fun signUp(email: String, password: String) {
        if (isFieldsValid(email, password)) {
            auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        _eventsLiveData.value = Event(AuthFragment.Events.REGISTRATION_SUCCESS)
                        repository.saveEmail(email)
                    } else {
                        _eventsLiveData.value = Event(AuthFragment.Events.REGISTRATION_FAILED)
                    }
                }
        }
    }

    fun signIn(email: String, password: String) {
        if (isFieldsValid(email, password)) {
            auth.signInWithEmailAndPassword(email, password).addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    _eventsLiveData.value = Event(AuthFragment.Events.REGISTRATION_SUCCESS)
                    repository.saveEmail(email)
                } else {
                    _eventsLiveData.value = Event(AuthFragment.Events.LOGIN_FAILED)
                }
            }
        }
    }

    private fun isFieldsValid(email: String, password: String): Boolean {
        var isValid = true
        when {
            TextUtils.isEmpty(email) || TextUtils.isEmpty(password) -> {
                _eventsLiveData.value = Event(AuthFragment.Events.AUTH_ALL_FIELDS_ERROR)
                isValid = false
            }
            isValidEmail(email).not() -> {
                _eventsLiveData.value = Event(AuthFragment.Events.AUTH_EMAIL_ERROR)
                isValid = false
            }
            isPasswordValid(password).not() -> {
                _eventsLiveData.value = Event(AuthFragment.Events.AUTH_PASSWORD_ERROR)
                isValid = false
            }
        }
        return isValid
    }
}