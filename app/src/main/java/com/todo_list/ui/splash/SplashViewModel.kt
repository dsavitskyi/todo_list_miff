package com.todo_list.ui.splash

import com.todo_list.architecture.base.BaseViewModel
import com.todo_list.architecture.events.Event
import kotlinx.coroutines.delay

class SplashViewModel : BaseViewModel<SplashFragment.Events>() {

    fun openNextScreen() {
        launchCoroutine({
            delay(SPLASH_SCREEN_DELAY)
            if (repository.getEmail().isNullOrBlank()) {
                _eventsLiveData.value = Event(SplashFragment.Events.FORWARD_REGISTER_SCREEN)
            } else {
                _eventsLiveData.value = Event(SplashFragment.Events.FORWARD_HOME_SCREEN)
            }
        })
    }

    companion object {
        const val SPLASH_SCREEN_DELAY = 2000L
    }
}