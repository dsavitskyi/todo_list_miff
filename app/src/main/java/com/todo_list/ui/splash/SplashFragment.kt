package com.todo_list.ui.splash

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.todo_list.R
import com.todo_list.app.Injector
import com.todo_list.architecture.base.BaseFragment

class SplashFragment : BaseFragment<SplashViewModel, SplashFragment.Events>() {

    override val viewModel: SplashViewModel by viewModels {
        Injector.provideSplashViewModelFactory()
    }

    override val layout by lazy { R.layout.splash_fragment }

    override fun onEvent(event: Events) {
        super.onEvent(event)
        when(event) {
            Events.FORWARD_REGISTER_SCREEN -> {
                findNavController().navigate(SplashFragmentDirections.actionSplashFragmentToAuthFragment())
            }
            Events.FORWARD_HOME_SCREEN -> {
                findNavController().navigate(SplashFragmentDirections.actionSplashFragmentToHomeFragment())
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.openNextScreen()
    }

    enum class Events {
        FORWARD_HOME_SCREEN,
        FORWARD_REGISTER_SCREEN
    }
}