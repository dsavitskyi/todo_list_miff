package com.todo_list.ui

import androidx.activity.viewModels
import com.todo_list.R
import com.todo_list.app.Injector
import com.todo_list.architecture.base.BaseActivity
import com.todo_list.architecture.extensions.gone
import com.todo_list.architecture.extensions.visible
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity<MainViewModel, MainActivity.Events>() {

    override val viewModel: MainViewModel by viewModels {
        Injector.provideMainViewModelFactory()
    }

    override val layout by lazy { R.layout.activity_main }

    override fun showLoading(show: Boolean) {
        if (show) {
            pb_loading?.visible()
        } else {
            pb_loading?.gone()
        }
    }

    enum class Events {}
}