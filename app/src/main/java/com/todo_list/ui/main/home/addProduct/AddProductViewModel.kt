package com.todo_list.ui.main.home.addProduct

import android.util.Log
import com.todo_list.app.Injector
import com.todo_list.architecture.base.BaseViewModel
import com.todo_list.architecture.events.Event
import com.todo_list.repository.database.ProductsDao
import com.todo_list.repository.models.ShoppingModel

class AddProductViewModel : BaseViewModel<AddProductDialogFragment.Events>() {

    private lateinit var productsDao: ProductsDao

    init {
        initDatabase(Injector.provideProductsDatabaseDao())
    }

    private fun initDatabase(productsDao: ProductsDao) {
        this.productsDao = productsDao
    }

    fun insertProduct(shopping: ShoppingModel) {
        launchCoroutine({
            productsDao.insertProduct(shopping)
            _eventsLiveData.value = Event(AddProductDialogFragment.Events.INSERT_SUCCESSFUL)
        }, {
            Log.d("Shopping", "Error adding product")
        })
    }
}