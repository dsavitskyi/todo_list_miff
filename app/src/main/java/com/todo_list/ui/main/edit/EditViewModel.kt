package com.todo_list.ui.main.edit

import android.util.Log
import com.todo_list.app.Injector
import com.todo_list.architecture.base.BaseViewModel
import com.todo_list.architecture.events.Event
import com.todo_list.repository.database.ProductsDao
import com.todo_list.repository.models.ShoppingModel

class EditViewModel : BaseViewModel<EditFragment.Events>() {

    private lateinit var productsDao: ProductsDao

    init {
        initDatabase(Injector.provideProductsDatabaseDao())
    }

    private fun initDatabase(productsDao: ProductsDao) {
        this.productsDao = productsDao
    }

    fun editProduct(shopping: ShoppingModel) {
        launchCoroutine({
            productsDao.updateProduct(shopping)
            _eventsLiveData.value = Event(EditFragment.Events.SAVED_SUCCESSFUL)
        }, {
            Log.d("Shopping", "Error adding product")
        })
    }
}