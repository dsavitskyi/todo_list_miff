package com.todo_list.ui.main.edit

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.todo_list.R
import com.todo_list.app.Injector
import com.todo_list.architecture.base.BaseFragment
import com.todo_list.architecture.extensions.afterTextChanged
import com.todo_list.architecture.extensions.toast
import com.todo_list.repository.models.ShoppingModel
import com.todo_list.utils.TextUtils
import kotlinx.android.synthetic.main.edit_fragment.*

class EditFragment : BaseFragment<EditViewModel, EditFragment.Events>() {

    val args: EditFragmentArgs by navArgs()

    override val viewModel: EditViewModel by viewModels {
        Injector.provideEditViewModelFactory()
    }

    override val layout by lazy { R.layout.edit_fragment }

    override fun onEvent(event: Events) {
        super.onEvent(event)
        when (event) {
            Events.SAVED_SUCCESSFUL -> {
                toast(getString(R.string.txt_changes_saved))
                findNavController().popBackStack()
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindUI()
    }

    private fun bindUI() {
        iv_backward.setOnClickListener { findNavController().popBackStack() }

        val currentProductDescription = args.product.title
        et_edit_product.afterTextChanged {
            if (currentProductDescription != it) {
                tv_save_product.setColorFilter(
                    ContextCompat.getColor(requireContext(), R.color.colorAccent),
                    android.graphics.PorterDuff.Mode.SRC_IN
                )
            } else {
                tv_save_product.setColorFilter(
                    ContextCompat.getColor(requireContext(), R.color.black),
                    android.graphics.PorterDuff.Mode.SRC_IN
                )
            }
        }

        et_edit_product.setText(currentProductDescription, TextView.BufferType.EDITABLE)
        val currentDate = TextUtils.formatDate()
        tv_save_product?.setOnClickListener {
            if (currentProductDescription != et_edit_product.text.toString()) {
                val product = ShoppingModel(
                    id = args.product.id, title = et_edit_product.text.toString(),
                    publishedAt = currentDate
                )
                viewModel.editProduct(product)
            }
        }
    }

    enum class Events {
        SAVED_SUCCESSFUL
    }
}