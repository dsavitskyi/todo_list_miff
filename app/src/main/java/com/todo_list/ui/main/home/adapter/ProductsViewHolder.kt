package com.todo_list.ui.main.home.adapter

import android.view.ViewGroup
import com.todo_list.R
import com.todo_list.architecture.base.BaseViewHolder
import com.todo_list.architecture.extensions.setThrottleOnClickListener
import com.todo_list.architecture.extensions.threadUnsafeLazy
import com.todo_list.repository.models.ShoppingModel
import kotlinx.android.synthetic.main.item_shopping.view.*

class ProductsViewHolder(
    parent: ViewGroup,
    private val listener: ProductsPagedAdapter.ProductsListListener
) : BaseViewHolder<ShoppingModel>(parent, R.layout.item_shopping, null) {

    private val tvProductTitle by threadUnsafeLazy { itemView.tv_product_name }
    private val tvProductDate by threadUnsafeLazy { itemView.tv_product_date }
    private val ivEdit by threadUnsafeLazy { itemView.iv_edit}
    private val ivDelete by threadUnsafeLazy { itemView.iv_delete}
    private val cvPreview by threadUnsafeLazy { itemView.cv_item_container}


    override fun onBind(item: ShoppingModel) {
        super.onBind(item)

        tvProductTitle.text = item.title
        tvProductDate.text = item.publishedAt

        ivEdit.setThrottleOnClickListener {
            listener.onEditClick(item)
        }

        ivDelete.setThrottleOnClickListener {
            listener.onDeleteClick(item)
        }

        cvPreview.setThrottleOnClickListener {
            listener.onPreview(item)
        }
    }
}