package com.todo_list.ui.main.home.addProduct

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.fragment.app.viewModels
import com.todo_list.R
import com.todo_list.app.Injector
import com.todo_list.architecture.base.BaseDialogFragment
import com.todo_list.architecture.extensions.toPx
import com.todo_list.architecture.extensions.toast
import com.todo_list.repository.models.ShoppingModel
import com.todo_list.utils.TextUtils
import kotlinx.android.synthetic.main.add_product_dialog_fragment.*

class AddProductDialogFragment :
    BaseDialogFragment<AddProductViewModel, AddProductDialogFragment.Events>() {

    override val viewModel: AddProductViewModel by viewModels {
        Injector.provideAddProductViewModelFactory()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return Dialog(requireContext(), theme)
    }

    override val layout by lazy { R.layout.add_product_dialog_fragment }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindUI()
    }

    override fun onEvent(event: Events) {
        super.onEvent(event)
        when (event) {
            Events.INSERT_SUCCESSFUL -> dismiss()
        }
    }

    override fun onResume() {
        super.onResume()
        dialog?.window?.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        if (dialog != null && dialog?.window != null) {
            val inset = InsetDrawable(
                ColorDrawable(Color.TRANSPARENT),
                8.toPx(),
                0,
                8.toPx(),
                0
            )
            dialog?.window?.setBackgroundDrawable(inset)
        }
    }

    private fun bindUI() {
        tv_add_to_list.setOnClickListener {
            if (et_add_goods.text.toString().isNotEmpty()) {
                val currentDate = TextUtils.formatDate()
                val product = ShoppingModel(title = et_add_goods.text.toString(), publishedAt = currentDate)
                viewModel.insertProduct(product)
            } else {
                toast(getString(R.string.txt_add_product_description))
            }
        }
    }

    enum class Events {
        INSERT_SUCCESSFUL
    }
}