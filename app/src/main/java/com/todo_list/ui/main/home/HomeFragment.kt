package com.todo_list.ui.main.home

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.todo_list.R
import com.todo_list.app.Injector
import com.todo_list.architecture.base.BaseFragment
import com.todo_list.architecture.extensions.invisible
import com.todo_list.architecture.extensions.visible
import com.todo_list.repository.models.ShoppingModel
import com.todo_list.ui.main.home.adapter.ProductsPagedAdapter
import kotlinx.android.synthetic.main.home_fragment.*

class HomeFragment : BaseFragment<HomeViewModel, HomeFragment.Events>() {

    private val productsPagedAdapter = ProductsPagedAdapter()

    override val viewModel: HomeViewModel by viewModels {
        Injector.provideHomeViewModelFactory()
    }

    override val layout by lazy { R.layout.home_fragment }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        productsPagedAdapter.productsListener = productsListener
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindUI()
        viewModel.getProducts()

        viewModel.productsLiveData.observe(viewLifecycleOwner, Observer { products ->
            productsPagedAdapter.submitList(products)
            viewModel.syncWithRealTimeDB(products)
            if (products.isEmpty())
                tv_add_product.visible()
            else
                tv_add_product.invisible()
        })
    }

    override fun logout() {
        super.logout()
        findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToAuthFragment())
    }

    private fun bindUI() {
        with(rv_shopping_items) {
            layoutManager = LinearLayoutManager(context)
            adapter = productsPagedAdapter
            itemAnimator = DefaultItemAnimator()
        }
        fb_add_good?.setOnClickListener {
            findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToAddProductDialogFragment())
        }
        ic_log_out?.setOnClickListener {
            viewModel.cleanDatabase()
        }
    }

    private val productsListener = object : ProductsPagedAdapter.ProductsListListener {
        override fun onEditClick(modelPart: ShoppingModel) {
            findNavController().navigate(HomeFragmentDirections
                .actionHomeFragmentToEditFragment(product = modelPart))
        }

        override fun onDeleteClick(modelPart: ShoppingModel) {
            viewModel.deleteProduct(modelPart)
        }

        override fun onPreview(modelPart: ShoppingModel) {
            findNavController().navigate(HomeFragmentDirections
                .actionHomeFragmentToPreviewFragment(product = modelPart))
        }
    }

    enum class Events {}
}