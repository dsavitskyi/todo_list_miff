package com.todo_list.ui.main.home

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.database.*
import com.todo_list.app.Injector
import com.todo_list.architecture.base.BaseViewModel
import com.todo_list.repository.database.ProductsDao
import com.todo_list.repository.models.ShoppingModel
import com.todo_list.repository.models.ShoppingModelFirebase
import com.todo_list.utils.Const

class HomeViewModel : BaseViewModel<HomeFragment.Events>() {

    lateinit var productsDao: ProductsDao
    private val userEmail = repository.getEmail()
    private var products = ArrayList<ShoppingModel>()
    var productsLiveData: LiveData<List<ShoppingModel>> = MutableLiveData()
    private var isProductItemDeleted = false

    private val productsDb: FirebaseDatabase = FirebaseDatabase.getInstance(Const.FIREBASE_DB_URL)
    private val productsRef: DatabaseReference =
        productsDb.reference.child(Const.TABLE_NAME).child(userEmail.replace(".", ""))

    init {
        initFBDatabase()
        initDatabase(Injector.provideProductsDatabaseDao())
    }

    private fun initDatabase(productsDao: ProductsDao) {
        this.productsDao = productsDao
    }

    private fun initFBDatabase() {
        productsRef.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {}

            override fun onDataChange(snapshot: DataSnapshot) {
                products.clear()
                for (postSnapshot in snapshot.children) {
                    val shopping: ShoppingModelFirebase = postSnapshot.getValue(
                        ShoppingModelFirebase::class.java
                    )!!
                    products.add(shopping.toShoppingRoomModel(true))
                }
                insertProducts(products)
            }
        })
    }

    fun syncWithRealTimeDB(products: List<ShoppingModel>) {
        val checkForSync = products.filter { !it.syncked }
        if (checkForSync.isNotEmpty() || isProductItemDeleted) {
            productsRef.setValue(products.map { it.toShoppingModelFifeBase() })
            isProductItemDeleted = false
        }
    }

    fun getProducts() {
        showLoading()
        productsLiveData = productsDao.getProducts()
        hideLoading()
    }

    fun insertProducts(products: List<ShoppingModel>) {
        launchCoroutine({
            productsDao.insertProducts(products)
        }, {
            Log.d("Shopping", "Error delete product")
        })
    }

    fun deleteProduct(product: ShoppingModel) {
        showLoading()
        launchCoroutine({
            isProductItemDeleted = true
            productsDao.deleteProduct(product)
            hideLoading()
        }, {
            hideLoading()
            isProductItemDeleted = false
            Log.d("Shopping", "Error delete product")
        })
    }

    fun cleanDatabase() {
        showLoading()
        launchCoroutine({
            productsDao.cleanDataBase()
            hideLoading()
            logout()
        }, {
            hideLoading()
            Log.d("Shopping", "Error clean database")
        })
    }
}