package com.todo_list.ui.main.preview

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.todo_list.R
import com.todo_list.app.Injector
import com.todo_list.architecture.base.BaseFragment
import kotlinx.android.synthetic.main.preview_fragment.*

class PreviewFragment : BaseFragment<PreviewViewModel, PreviewFragment.Events>() {

    val args: PreviewFragmentArgs by navArgs()

    override val viewModel: PreviewViewModel by viewModels {
        Injector.providePreviewViewModelFactory()
    }

    override val layout by lazy { R.layout.preview_fragment }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindUI()
    }

    private fun bindUI() {
        iv_backward.setOnClickListener { findNavController().popBackStack() }
        tv_preview_product.text = args.product.title
    }

    enum class Events {}
}