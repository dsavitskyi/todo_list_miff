package com.todo_list.utils

import android.util.Patterns
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

object TextUtils {

    fun isValidEmail(email: String?): Boolean {
        return !email.isNullOrBlank() && Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    private val EMAIL_ADDRESS_PATTERN: Pattern = Pattern.compile(
        "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                "\\@" +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                "(" +
                "\\." +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                ")+"
    )

    fun isEmailValidCustom(email: String?): Boolean {
        return !email.isNullOrBlank() && EMAIL_ADDRESS_PATTERN.matcher(email).matches()
    }

    fun isPasswordValid(value: String): Boolean {
        if(value.length < 8) return false
        if(value.matches(Regex(".*[a-z A-Z].*")).not()) return false
        if(value.matches(Regex(".*[0-9].*")).not()) return false
        return true
    }

    fun formatDate(): String {
        val currentDate = Calendar.getInstance().time
        val df = SimpleDateFormat(Const.DATE_PATTERN, Locale.getDefault())
        return df.format(currentDate)
    }
}