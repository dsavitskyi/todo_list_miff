package com.todo_list.utils

object Const {
    const val FIREBASE_DB_URL = "https://todoshopinglist.firebaseio.com/"
    const val TABLE_NAME = "products"
    const val DATE_PATTERN = "dd MM yyyy HH:mm"
}