package com.todo_list.app

import com.todo_list.app.App.Companion.applicationContext
import com.todo_list.repository.database.ProductsDao
import com.todo_list.repository.database.ProductsDatabase
import com.todo_list.ui.MainViewModelFactory
import com.todo_list.ui.auth.AuthViewModelFactory
import com.todo_list.ui.main.edit.EditViewModelFactory
import com.todo_list.ui.main.home.HomeViewModelFactory
import com.todo_list.ui.main.home.addProduct.AddProductViewModelFactory
import com.todo_list.ui.main.preview.PreviewViewModelFactory
import com.todo_list.ui.splash.SplashViewModelFactory

object Injector {

    fun provideSplashViewModelFactory() =
        SplashViewModelFactory()

    fun provideMainViewModelFactory() =
        MainViewModelFactory()

    fun provideRegisterViewModelFactory() =
        AuthViewModelFactory()

    fun provideHomeViewModelFactory() =
        HomeViewModelFactory()

    fun provideAddProductViewModelFactory() =
        AddProductViewModelFactory()

    fun provideProductsDatabase(): ProductsDatabase =
        ProductsDatabase.getInstance(applicationContext)

    fun provideProductsDatabaseDao(): ProductsDao =
        provideProductsDatabase().productsDao()

    fun provideEditViewModelFactory() =
        EditViewModelFactory()

    fun providePreviewViewModelFactory() =
        PreviewViewModelFactory()
}