package com.todo_list.app

import android.app.Application
import android.content.Context
import androidx.lifecycle.LifecycleObserver

class App : Application(), LifecycleObserver {

    init {
        instance = this
    }

    companion object {
        private var instance: App? = null

        val applicationContext: Context
            get() = instance!!.applicationContext
    }
}