package com.todo_list.repository

import com.todo_list.repository.local.SettingsRepository
import com.todo_list.repository.local.SharedPreferencesRepository

object Repository : SettingsRepository {

    private val settingsRepository: SettingsRepository = SharedPreferencesRepository

    override fun getEmail(): String {
        return settingsRepository.getEmail()
    }

    override fun saveEmail(email: String) {
        settingsRepository.saveEmail(email)
    }

    override fun clear() {
        settingsRepository.clear()
    }
}