package com.todo_list.repository.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.todo_list.repository.models.ShoppingModel

@Dao
interface ProductsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertProduct(products: ShoppingModel)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertProducts(products: List<ShoppingModel>)

    @Delete
    suspend fun deleteProduct(product: ShoppingModel)

    @Query("DELETE FROM products")
    suspend fun cleanDataBase()

    @Update
    suspend fun updateProduct(product: ShoppingModel)

    @Query("SELECT * FROM products")
    fun getProducts(): LiveData<List<ShoppingModel>>

    @Query("SELECT * FROM products")
    suspend fun getAllProducts(): List<ShoppingModel>
}