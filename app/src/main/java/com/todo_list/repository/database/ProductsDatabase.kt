package com.todo_list.repository.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.todo_list.repository.database.ProductsDatabase.Companion.DATABASE_VERSION
import com.todo_list.repository.models.ShoppingModel

@Database(
    entities = [
        ShoppingModel::class
    ],
    version = DATABASE_VERSION,
    exportSchema = false
)

abstract class ProductsDatabase : RoomDatabase() {
    abstract fun productsDao(): ProductsDao

    companion object {
        const val DATABASE_VERSION = 1

        @Volatile private var INSTANCE: ProductsDatabase? = null

        fun getInstance(context: Context): ProductsDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context.applicationContext,
                ProductsDatabase::class.java, "Products.db")
                .fallbackToDestructiveMigration()
                .build()
    }
}