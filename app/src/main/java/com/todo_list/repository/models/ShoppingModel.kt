package com.todo_list.repository.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "products")
data class ShoppingModel(
    @PrimaryKey (autoGenerate = true)
    val id: Int? = null,
    val title: String = "",
    val publishedAt: String = "",
    val syncked: Boolean = false
):Serializable {
    fun toShoppingModelFifeBase() =
        ShoppingModelFirebase(
            id = id,
            title = title,
            publishedAt = publishedAt
        )
}

data class ShoppingModelFirebase (
    val id: Int? = null,
    val title: String = "",
    val publishedAt: String = ""
) {
    fun toShoppingRoomModel(sync: Boolean) =
        ShoppingModel(
            id = id,
            title = title,
            publishedAt = publishedAt,
            syncked = sync
        )
}


