package com.todo_list.repository.local

interface SettingsRepository {

    fun getEmail(): String
    fun saveEmail(email: String)
    fun clear()
}