package com.todo_list.repository.local

import android.content.Context
import android.content.SharedPreferences
import com.todo_list.app.App

private const val SETTINGS_FILE = "com.shoppingList.settingsFile"
private const val PREF_KEY_USER_EMAIL = "PREF_KEY_USER_EMAIL"

object SharedPreferencesRepository : SettingsRepository {

    fun getSharedPreferences(): SharedPreferences {
        return App.applicationContext.getSharedPreferences(SETTINGS_FILE, Context.MODE_PRIVATE)
    }

    override fun getEmail(): String {
        return getSharedPreferences()
            .getString(PREF_KEY_USER_EMAIL, "") ?: ""
    }

    override fun saveEmail(email: String) {
        with(
            getSharedPreferences()
                .edit()
        ) {
            putString(PREF_KEY_USER_EMAIL, email)
            apply()
        }
    }

    override fun clear() {
        with(
            getSharedPreferences()
                .edit()
        ) {
            clear()
            apply()
        }
    }
}