package com.todo_list

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.navigation.Navigation
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.todo_list.ui.auth.AuthFragment
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class NavigationFragmentTest {
    @Test
    fun testNavigationToAuthScreen() {

        val navController = TestNavHostController(
            ApplicationProvider.getApplicationContext())
        navController.setGraph(R.navigation.nav_graph)

        val titleScenario = launchFragmentInContainer<AuthFragment>()
        onView(withId(R.id.email_edt_text))
            .perform(ViewActions.typeText("rob@gmail.com"))
        onView(withId(R.id.pass_edt_text))
            .perform(ViewActions.typeText("12345678qw"))

        titleScenario.onFragment { fragment ->
            Navigation.setViewNavController(fragment.requireView(), navController)
        }
        onView(withId(R.id.signup_btn)).perform(click())
    }
}