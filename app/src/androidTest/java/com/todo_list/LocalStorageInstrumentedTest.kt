package com.todo_list

import android.content.Context
import android.content.Intent
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.todo_list.app.Injector
import com.todo_list.repository.local.SharedPreferencesRepository
import com.todo_list.repository.models.ShoppingModel
import com.todo_list.ui.MainActivity
import junit.extensions.ActiveTestSuite
import kotlinx.android.synthetic.main.register_fragment.*
import kotlinx.coroutines.*

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import org.junit.Rule

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class LocalStorageInstrumentedTest {

    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("com.todo_list", appContext.packageName)
    }

    @Test
    fun saveAndReadValues() {
        val appContext: Context = InstrumentationRegistry.getInstrumentation().context
        //check if value saves correctly
        val emailForSave = "test@mail.com"
        val sharedPref = SharedPreferencesRepository
        sharedPref.getSharedPreferences()
        sharedPref.saveEmail(emailForSave)
        val readValue = sharedPref.getEmail()
        assertTrue(readValue == emailForSave)
    }

    @Test
    fun checkDBProductInsert() {
        val products = listOf<ShoppingModel>(
            ShoppingModel(1, "bread and honey", "15.09.2020"),
            ShoppingModel(2, "milk and butter", "16.09.2020")
        )
        val dbProducts: ArrayList<ShoppingModel> = ArrayList()
        val dbDao = Injector.provideProductsDatabaseDao()
        runBlocking {
            dbDao.insertProduct(products[0])
            dbProducts.addAll(dbDao.getAllProducts())
        }
        assertTrue(products.get(0) == dbProducts.get(0))
    }

    @Test
    fun checkDBProductInsertLiveData() {
        val products = listOf<ShoppingModel>(
            ShoppingModel(1, "bread and honey", "15.09.2020"),
            ShoppingModel(2, "milk and butter", "16.09.2020")
        )
        val dbDao = Injector.provideProductsDatabaseDao()
        var db = MutableLiveData<List<ShoppingModel>>() as LiveData<List<ShoppingModel>>

        CoroutineScope(Dispatchers.Main).launch {

            dbDao.insertProduct(products[0])

            val observer = Observer<List<ShoppingModel>> {
                assertTrue(products.get(0) == it.get(0))
            }
            try {
                db.observeForever(observer)
                db = dbDao.getProducts()
            } finally {
               db.removeObserver(observer)
            }
        }
    }

    @Test
    fun checkDBUpdate() {
        val dbProducts: ArrayList<ShoppingModel> = ArrayList()
        val product = ShoppingModel(5, "milk and butter", "16.09.2020")
        val dbDao = Injector.provideProductsDatabaseDao()
        runBlocking {
            dbDao.insertProduct(product)
            val productCopy = product.copy(title = "tea and candy", publishedAt = "17.09.2020")
            dbDao.updateProduct(productCopy)
            dbProducts.addAll(dbDao.getAllProducts())
        }
        assertTrue(product.id == dbProducts.get(dbProducts.size - 1).id)
    }

    @Test
    fun checkDBInsertAll() {
        val dbProducts: ArrayList<ShoppingModel> = ArrayList()
        val products = listOf<ShoppingModel>(
            ShoppingModel(1, "bread and honey", "15.09.2020"),
            ShoppingModel(2, "milk and butter", "16.09.2020"),
            ShoppingModel(3, "tea and candy", "16.09.2020"),
            ShoppingModel(4, "water and ice", "16.09.2020")
        )
        val dbDao = Injector.provideProductsDatabaseDao()
        runBlocking {
            dbDao.insertProducts(products)
            dbProducts.addAll(dbDao.getAllProducts())
        }
        assertTrue(products.get(0) == dbProducts.get(0))
    }

    @Test
    fun checkDBDeleteAll() {
        val dbProducts: ArrayList<ShoppingModel> = ArrayList()
        val products = listOf<ShoppingModel>(
            ShoppingModel(1, "bread and honey", "15.09.2020"),
            ShoppingModel(2, "milk and butter", "16.09.2020"),
            ShoppingModel(3, "tea and candy", "16.09.2020"),
            ShoppingModel(4, "water and ice", "16.09.2020")
        )
        val dbDao = Injector.provideProductsDatabaseDao()
        runBlocking {
            dbDao.insertProducts(products)
            dbDao.cleanDataBase()
            dbProducts.addAll(dbDao.getAllProducts())
        }
        assertTrue(dbProducts.isEmpty())
    }
}