package com.todo_list

import android.os.Bundle
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.todo_list.repository.models.ShoppingModel
import com.todo_list.ui.main.edit.EditFragment
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class EditFragmentTest {

    @Test
    fun testEditAbility() {
        val title = "some text to edit"
        val bundle = Bundle()
        bundle.putSerializable("product", ShoppingModel(title = title))
        val scenario = launchFragmentInContainer<EditFragment>(fragmentArgs = bundle)
        onView(withId(R.id.et_edit_product))
            .perform(typeText(title))
        onView(withId(R.id.tv_save_product)).perform(click())
    }
}