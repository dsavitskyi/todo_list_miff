package com.todo_list

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.todo_list.ui.auth.AuthFragment
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class AuthFragmentTest {

    @Test
    fun testAuth() {
        val scenario = launchFragmentInContainer<AuthFragment>()
        onView(withId(R.id.email_edt_text))
            .perform(ViewActions.typeText("rob@gmail.com"))
        onView(withId(R.id.pass_edt_text))
            .perform(ViewActions.typeText("12345678qw"))
        onView(withId(R.id.signup_btn)).perform(ViewActions.click())
    }
}