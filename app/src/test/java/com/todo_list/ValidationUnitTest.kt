package com.todo_list

import com.todo_list.repository.models.ShoppingModel
import com.todo_list.utils.TextUtils
import org.junit.Assert.assertTrue
import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ValidationUnitTest {

    @Test
    fun validateEmail() {
        assertTrue(TextUtils.isValidEmail("dev@gmail.com"))
    }

    @Test
    fun validateEmailCustomPattern() {
        assertTrue(TextUtils.isEmailValidCustom("dev@gmail.com"))
    }

    @Test
    fun validatePassword() {
        assertTrue(TextUtils.isPasswordValid("w14gtA78ur"))
    }

    @Test
    fun createProductModelFirebase() {
        val product = ShoppingModel(1, "Dr.Pepper", "15.09.2020", false)
        print(product.toShoppingModelFifeBase())
    }

    @Test
    fun formatDate() {
        print(TextUtils.formatDate())
    }
}