package com.todo_list

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.todo_list.architecture.events.Event
import com.todo_list.ui.splash.SplashFragment
import com.todo_list.ui.splash.SplashViewModel
import org.hamcrest.CoreMatchers.not
import org.hamcrest.CoreMatchers.nullValue
import org.junit.Test

import org.junit.Assert.*
import org.junit.Rule

class SplashViewModelTest {
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Test
    fun openNextScreen() {

        val splashViewModel = SplashViewModel()

        val observer = Observer<Event<SplashFragment.Events>> {}
        try {

            splashViewModel.eventsLiveData.observeForever(observer)

            splashViewModel.openNextScreen()

            val value = splashViewModel.eventsLiveData.value
            assertThat(value?.getContentIfNotHandled(), (not(nullValue())))

        } finally {
            splashViewModel.eventsLiveData.removeObserver(observer)
        }
    }
}